import sys
from functools import reduce


def add_ten(in_a):
    return in_a + 10


print(f'add_ten(5) = {add_ten(5)}')


def sumlist(l):
    sum = 0
    for val in l:
        sum += val
    return sum


print(f'sumlist([1,2,3,4,5]) = {sumlist([1,2,3,4,5])}')


def sumlist_reduce(l):
    def mass_addition(accum, val):
        return accum + val
    return reduce(mass_addition, l)


print(f'sumlist_reduce([1,2,3,4,5]) = {sumlist_reduce([1,2,3,4,5])}')


def maxlist(l):
    max = sys.float_info.max * (-1)
    for val in l:
        if val > max:
            max = val
    return max


print(f'maxlist([4,5,1,7,1]) = {maxlist([4,5,1,7,1])}')


def maxlist_reduce(l):
    def max_value(a, b):
        if a > b:
            return a
        else:
            return b
    return reduce(max_value, l)


print(f'maxlist_reduce([4,5,1,7,1]) = {maxlist_reduce([4,5,1,7,1])}')


def numdigits(a):
    if abs(a) < 10:
        return 1
    return numdigits(abs(a) // 10) + 1


print(f'numdigits(12345) = {numdigits(12345)}')
print(f'numdigits(-12345) = {numdigits(-12345)}')


def numcat(a, b):
    return a * (10 ** numdigits(b)) + b


print(f'numcat(123, 456) = {numcat(123, 456)}')


def reverselist(l):
    out_l = []
    for i in range(len(l), 0, -1):
        out_l.append(l[i - 1])
    return out_l


print(f'reverselist([1,2,3,4,5]) = {reverselist([1,2,3,4,5])}')

roman_to_decadic_dict = {
    'I': 1,
    'V': 5,
    'X': 10,
    'L': 50,
    'C': 100,
    'D': 500,
    'M': 1000,
}

decadic_to_roman_dict = {
    1: 'I',
    5: 'V',
    10: 'X',
    50: 'L',
    100: 'C',
    500: 'D',
    1000: 'M',
}

roman_lower_dict = {
    'V': 'I',
    'X': 'I',
    'L': 'X',
    'C': 'X',
    'D': 'C',
    'M': 'C',
}

decadic_lower_dict = {
    5: 1,
    10: 1,
    50: 10,
    100: 10,
    500: 100,
    1000: 100,
}


def roman_to_decadic(roman):
    decadic = []
    for c in roman:
        decadic.append(roman_to_decadic_dict[c])
    for i in range(len(decadic) - 1):
        if decadic[i + 1] > decadic[i]:
            decadic[i] *= -1
    return sumlist(decadic)


print(f'roman_to_decadic("MCMLXXXIX")={roman_to_decadic("MCMLXXXIX")}')


def roman_to_decadic_map(roman):
    decadic = list(map(lambda v: roman_to_decadic_dict[v], roman))
    for i in range(len(decadic) - 1):
        if decadic[i + 1] > decadic[i]:
            decadic[i] *= -1
    return sumlist(decadic)


print(f'roman_to_decadic_map("MCMLXXXIX")={roman_to_decadic_map("MCMLXXXIX")}')


def decadic_to_roman(decadic):
    if decadic < 1:
        return ""
    levels = list(decadic_to_roman_dict.keys())
    current_level_index = len(levels) - 1
    while (decadic // levels[current_level_index] < 1):
        current_level_index -= 1
    decade = levels[current_level_index]
    roman_out = ""
    for i in range(decadic // decade):
        roman_out += decadic_to_roman_dict[decade]
    remainder = decadic % decade
    if (decade in decadic_lower_dict and
            remainder >= (decade - decadic_lower_dict[decade])):
        roman_out += roman_lower_dict[decadic_to_roman_dict[decade]]
        roman_out += decadic_to_roman_dict[decade]
        remainder = remainder - (decade - decadic_lower_dict[decade])
    return roman_out + decadic_to_roman(remainder)


print(f'decadic_to_roman(1989)={decadic_to_roman(1989)}')


def add_roman(a, b):
    return decadic_to_roman(roman_to_decadic(a) + roman_to_decadic(b))


print(f'add_roman("MCMLXXXII", "XXXV")={add_roman("MCMLXXXII", "XXXV")}')
